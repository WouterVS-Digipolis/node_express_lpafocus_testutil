/*jslint node: true */
'use strict';

var _ = require('lodash');
var EventEmitter = require('events').EventEmitter;

var testActions = function(constants) {
  return {
    /**
     * Create a fake event
     * @param  {[type]} source     one of the constants.PayloadSources (BOOT, VIEW of SERVER)
     * @param  {[type]} actionType a constants.ActionTypes constant
     * @param  {[type]} payload    extra payload for the event
     */
    createTestAction: function(source, actionType, payload) {
      return {
        source: source,
        action: _.extend({
          type: actionType
        }, payload || {})
      };
    },

    /**
     * create a fake event triggered by the USER (view/jsx)
     * @param  {string}: actionType: the action constant
     * @param  {object}: payload: extra payload. defaults to {}.
     */
    createViewTestAction: function(actionType, payload) {
      return this.createTestAction(constants.PayloadSources.VIEW, actionType, payload);
    },

    /**
     * create a fake event triggered by the FOCUS CONTAINER at startup
     * @param  {string} actionType: the action constant
     * @param  {object} payload: extra payload. defaults to {}.
     */
    createBootTestAction: function(actionType, payload) {
      return this.createTestAction(constants.PayloadSources.BOOT, actionType, payload);
    },

    /**
     * create a fake event triggered by the BACKEND FOCUS CONTAINER
     * @param  {string} actionType: the action constant
     * @param  {object} payload: extra payload. defaults to {}.
     */
    createServerTestAction: function(actionType, payload) {
      return this.createTestAction(constants.PayloadSources.SERVER, actionType, payload);
    }
  };
};

/**
 * Create a plugin/app store
 * @param  {Object} Extra functions for the store
 */
function createAppStore(storeExtensions) {
  return _.extend({
    emitChange: function() {
      this.emit('change');
    },
    addChangeListener: function(callback) {
      this.on('change', callback);
    },
    removeChangeListener: function(callback) {
      this.removeListener('change', callback);
    }
  }, storeExtensions, EventEmitter.prototype);
}

function focusTestUtil(constants) {
  return {
    actions: testActions(constants),
    createAppStore: createAppStore
  };
}

module.exports = focusTestUtil;