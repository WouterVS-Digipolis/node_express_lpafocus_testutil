FocusTestUtil
=============

**Installatie:**  
`npm install --save-dev git+https://WouterVS-Digipolis@bitbucket.org/WouterVS-Digipolis/node_express_lpafocus_testutil.git`

**Update package.json:**
```
#!json  
"jest": {
    "unmockedModulePathPatterns": ["react", "FocusTestUtil"]
},
```

**Usage:**

```
#!js

var constants = require('./focusContainer/constants.js');  
var focusTest = require('FocusTestUtil')(constants);

// Create a default store with an extra public function 'yourFunction' 
var appStore = focusTest.createAppStore({
	yourFunction: function() {
    	return [1, 2, 3];
    }
});

// Create container actions:
var userAction = focusTest.actions.createViewTestAction(
	constants.actionTypes.SOME_ACTION, {
		param1: true,
		param2: "extra payload"
	}
);

// Parameters are the same as createViewTestAction:
var bootaction = focusTest.actions.createBootTestAction(...);
var serverAction = focusTest.actions.createServerTestAction(...);
```  


